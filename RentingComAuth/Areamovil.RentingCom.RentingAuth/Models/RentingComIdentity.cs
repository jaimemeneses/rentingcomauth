﻿using System;

namespace Areamovil.RentingCom.RentingAuth.Models {
  /// <summary>
  /// Represents a RentingCom user who has been authenticated from a JWT token
  /// </summary>
  public class RentingComIdentity {
    /// <summary>
    /// The user's given name.
    /// </summary>
    public string Name { get; }
    /// <summary>
    /// The user's email address.
    /// This property must have the first value of the "emails" JWT claim.
    /// </summary>
    public string EmailAddress { get; }
    /// <summary>
    /// The user's Id in the B2C repository.
    /// Do NOT use any other form of Id to store a reference to this user.
    /// </summary>
    public string UserId { get; }
    /// <summary>
    /// The user's role in the platform.
    /// </summary>
    public RentingComRole Role { get; }

    public RentingComIdentity(string name, string emailAddress, 
                              string userId, RentingComRole role) {
      Name = name ?? throw new ArgumentNullException(nameof(name));
      EmailAddress = emailAddress ?? throw new ArgumentNullException(nameof(emailAddress));
      UserId = userId ?? throw new ArgumentNullException(nameof(userId));
      Role = role;
    }
  }
}
