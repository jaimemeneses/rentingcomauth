﻿namespace Areamovil.RentingCom.RentingAuth.Models {
  public enum RentingComRole {
    /// <summary>
    /// End user of the application. This role has no special permissions.
    /// </summary>
    EndUser,
    /// <summary>
    /// A customer support service agent.
    /// </summary>
    ServiceAgent,
    /// <summary>
    /// Aids the user in the used vehicles purchasing process by guiding him
    /// with general vehicle knowledge questions and overseeing his drive tests.
    /// </summary>
    SalesRep,
    /// <summary>
    /// Performs administration and overseeing duties.
    /// </summary>
    AdminAssistant
  }
}
