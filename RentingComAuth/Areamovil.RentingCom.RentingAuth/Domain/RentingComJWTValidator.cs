﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using Areamovil.RentingCom.RentingAuth.Domain.Exceptions;
using Areamovil.RentingCom.RentingAuth.Models;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Cryptography;
using System.Collections.Generic;
using Areamovil.RentingCom.RentingAuth.Domain.ADB2C;

namespace Areamovil.RentingCom.RentingAuth.Domain
{
    public class RentingComJWTValidator : IRentingComJWTValidator
    {
        private readonly IB2CValidationConfiguration iB2CValidationConfiguration;
        private readonly IB2CValidationKeysFactory iB2CValidationKeysFactory;
        //private ICollection<RsaSecurityKey> dictionary;
        public RentingComJWTValidator(IB2CValidationKeysFactory iB2CValidationKeysFactory, IB2CValidationConfiguration iB2CValidationConfiguration)
        {
            this.iB2CValidationConfiguration = iB2CValidationConfiguration ?? throw new RentingComJWTValidationException(nameof(iB2CValidationConfiguration));
            this.iB2CValidationKeysFactory = iB2CValidationKeysFactory ?? throw new RentingComJWTValidationException(nameof(iB2CValidationKeysFactory));
        }
        public async Task<RentingComIdentity> ValidateJWTTokenAsync(string jwtText)
        {
            RentingComIdentity rentingComIdentity = null;
            if (jwtText == null)
            {
                throw new RentingComJWTValidationException(nameof(jwtText));
            }
            try
            {
                rentingComIdentity = await ValidateJWTToken(jwtText);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception validating token");
                Debug.WriteLine(ex);

                throw new RentingComJWTValidationException("Exception validating token", ex);
            }
            return rentingComIdentity;
        }

        private async Task<RentingComIdentity> ValidateJWTToken(string jwtToken)
        {
            if (string.IsNullOrEmpty(jwtToken))
            {
                throw new ArgumentNullException(nameof(jwtToken));
            }
            
            string sub;
            string name;
            string emailAddress;
            RentingComRole role = RentingComRole.EndUser;

            var keys = await iB2CValidationKeysFactory.GetKeysAsync();


            var tokenParameters = new TokenValidationParameters
            {
                ValidAudiences = iB2CValidationConfiguration.B2CAudiences,
                ValidIssuer = iB2CValidationConfiguration.B2CTokenIssuer,
                ValidateAudience = true,
                ValidateIssuer = true,
                IssuerSigningKeys = keys,
                RequireExpirationTime = true,
                ValidateLifetime = false,// solo para pruebas -> esto permite usar tokens expirados
                RequireSignedTokens = true
            };
            SecurityToken validatedToken = null;
            var jwtHandler = new JwtSecurityTokenHandler();
            var principal = jwtHandler.ValidateToken(jwtToken, tokenParameters, out  validatedToken);
            string payload=null;
            try
            {
                payload = validatedToken.ToString().Replace("}.{", "}}.{{").Split("}.{")[1];
            }
            catch (Exception) {
                throw new RentingComJWTValidationException("Invalid token");
            }
            try { 
                JObject json = JObject.Parse(payload);
                name = json.Value<string>("name");
                emailAddress = (string)json.GetValue("emails").First;
                sub = json.Value<string>("sub");
                string rc_role = json.Value<string>("rc_role");
                if (rc_role != null)
                {
                    if (rc_role.Equals("serviceAgent"))
                    {
                        role = RentingComRole.ServiceAgent;
                    }
                    else if (rc_role.Equals("salesRep"))
                    {
                        role = RentingComRole.SalesRep;
                    }
                    else if (rc_role.Equals("adminAsistant"))
                    {
                        role = RentingComRole.AdminAssistant;
                    }
                    else if (rc_role.Equals("endUser"))
                    {
                        role = RentingComRole.EndUser;
                    }                  
                }
                else
                {
                    //throw new RentingComJWTValidationException(nameof(rc_role));
                }
                return new RentingComIdentity(name, emailAddress, sub, role);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                throw new RentingComJWTValidationException("Error validating JWT", ex);
            }
            
        }       
    }
}



