﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Areamovil.RentingCom.RentingAuth.Domain.ADB2C
{
    public static class ADB2CValidationServiceCollectionExtensions
    {
        /// <summary>
        /// Configures the dependency container with the appropiate
        /// JWT validation interface implementation and it's supporting classes.
        /// </summary>
        /// <param name="services">The dependency container for the application</param>
        /// <param name="b2cConfig">
        /// The B2C configuration values. <para/>
        /// It is up to each application to determine how best to
        /// implement this interface.
        /// </param>
        public static void AddADB2CRentingComJWTValidator(this IServiceCollection services,
                                                          IB2CValidationConfiguration b2cConfig)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }
            if (b2cConfig == null)
            {
                throw new ArgumentNullException(nameof(b2cConfig));
            }
            services.AddTransient(_ => b2cConfig);
            services.AddSingleton<IRentingComJWTValidator, RentingComJWTValidator>();
            services.AddSingleton<IB2CValidationKeysFactory, B2CValidationKeysFactory>();            
        }
    }
}
