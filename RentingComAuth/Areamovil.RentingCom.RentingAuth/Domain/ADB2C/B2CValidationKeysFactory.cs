﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Areamovil.RentingCom.RentingAuth.Domain.Exceptions;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Areamovil.RentingCom.RentingAuth.Domain.ADB2C
{
    internal class B2CValidationKeysFactory : IB2CValidationKeysFactory
    {
        private readonly IB2CValidationConfiguration authConfig;
        private IEnumerable<RsaSecurityKey> cachedKeys;
        private DateTime cachedKeysEvictionDate;

        public B2CValidationKeysFactory(IB2CValidationConfiguration authConfig)
        {
            this.authConfig = authConfig ?? throw new ArgumentNullException(nameof(authConfig));
        }
        public async Task<IEnumerable<RsaSecurityKey>> GetKeysAsync()
        {
             if (cachedKeys!=null && cachedKeys.Any() && DateTime.Now < cachedKeysEvictionDate) {
                 return cachedKeys;
             }
            var request = new HttpRequestMessage(HttpMethod.Get, authConfig.B2CKeysUrl);
            HttpResponseMessage jwksResponse = null;
            string jwksText;
            List<RsaSecurityKey> tempKeys = new List<RsaSecurityKey>();
            using (var httpClient = new HttpClient())
            {
                var jwksRequest = new HttpRequestMessage(HttpMethod.Get, authConfig.B2CKeysUrl);
                jwksResponse = await httpClient.SendAsync(jwksRequest);
                jwksResponse.EnsureSuccessStatusCode();

                jwksText = await jwksResponse.Content.ReadAsStringAsync();
            }

            if (!jwksResponse.IsSuccessStatusCode)
            {
                throw new RentingComJWTValidationException("Exception status code: "+jwksResponse.StatusCode);
            }

            dynamic jwksObject = JObject.Parse(jwksText);
           
            var keyObject2 = jwksObject["keys"];
            if (keyObject2 != null)
            {
                for (int i = 0; i < keyObject2.Count; i++)
                {
                    var keyObject = keyObject2[i];
                    var encodedModulus = (string)keyObject["n"];
                    var encodedExponent = (string)keyObject["e"];

                    var keyModulus = DecodeBase64Url(encodedModulus);
                    var keyExponent = DecodeBase64Url(encodedExponent);

                    var keyParams = new RSAParameters
                    {
                        Modulus = keyModulus,
                        Exponent = keyExponent
                    };

                    var rsaKey = RSA.Create();
                    rsaKey.ImportParameters(keyParams);

                    var tokenKey = new RsaSecurityKey(rsaKey);
                    tempKeys.Add(tokenKey);
                }
            }                
           
            var secondsToEviction = 0;
            var cacheControlHeader = jwksResponse.Headers.CacheControl;

            if (cacheControlHeader != null)
            {
                var maxAgeKey = cacheControlHeader.MaxAge;
                if (maxAgeKey.HasValue)
                {
                    secondsToEviction = (int)maxAgeKey.Value.TotalSeconds;
                }
            }

            cachedKeysEvictionDate = DateTime.Now.AddSeconds(secondsToEviction);
            cachedKeys = tempKeys;
            return cachedKeys;
        }
        byte[] DecodeBase64Url(string base64Url)
        {
            var padded = base64Url.Length % 4 == 0
                ? base64Url : base64Url + "====".Substring(base64Url.Length % 4);

            var base64 = padded.Replace("_", "/").Replace("-", "+");

            return Convert.FromBase64String(base64);
        }
    }
}