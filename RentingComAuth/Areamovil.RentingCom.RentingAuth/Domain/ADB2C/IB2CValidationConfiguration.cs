﻿using System.Collections.Generic;

namespace Areamovil.RentingCom.RentingAuth.Domain.ADB2C
{
    /// <summary>
    /// This interface decouples the JWT verification 
    /// algorithms from the configuration system for the
    /// application. <para/>
    /// It is up to the application to implement
    /// this interface in the manner it best sees fit to do so.
    /// </summary>
    public interface IB2CValidationConfiguration
    {
        /// <summary>
        /// The B2C application Ids that are allowed to access this
        /// application.
        /// </summary>
        IEnumerable<string> B2CAudiences { get; }
        /// <summary>
        /// The B2C issuer Id. <para />
        /// 
        /// See https://docs.microsoft.com/en-us/azure/active-directory-b2c/active-directory-b2c-reference-tokens
        /// </summary>
        string B2CTokenIssuer { get; }
        /// <summary>
        /// A URL that contains the set of keys that B2C can use to
        /// sign valid tokens for the application, this value comes
        /// from the jwks_uri attribute of the B2C metadata. <para />
        /// 
        /// See https://docs.microsoft.com/en-us/azure/active-directory-b2c/active-directory-b2c-reference-tokens
        /// </summary>
        string B2CKeysUrl { get; }
    }
}
