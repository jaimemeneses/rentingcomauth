﻿using System;

namespace Areamovil.RentingCom.RentingAuth.Domain.Exceptions {
  /// <summary>
  /// Represents an error validating the JWT token or any of it's claims
  /// </summary>
  public class RentingComJWTValidationException : Exception {
    /// <summary>
    /// Constructs a JWT validation exception with a friendly message.
    /// </summary>
    /// <param name="message">A message with context about the exception.</param>
    public RentingComJWTValidationException(string message) : base(message) { }
    /// <summary>
    /// Constructs a JWT validation exception with a friendly message and
    /// an inner exception.
    /// </summary>
    /// <param name="message">A message with context about the exception.</param>
    /// <param name="innerException">The exception that caused this exception.</param>
    public RentingComJWTValidationException(string message, Exception innerException) : base(message, innerException) { }
  }
}
