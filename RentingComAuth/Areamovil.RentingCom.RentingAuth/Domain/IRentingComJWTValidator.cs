﻿using Areamovil.RentingCom.RentingAuth.Models;
using System.Threading.Tasks;

namespace Areamovil.RentingCom.RentingAuth.Domain {
  /// <summary>
  /// Validates B2C JWT tokens, this is the main gateway into the 
  /// RentingCom authentication and authorization process
  /// </summary>
  public interface IRentingComJWTValidator {
    /// <summary>
    /// Decodes a B2C JWT token into a RentingComIdentity object
    /// </summary>
    /// <exception cref="Exceptions.RentingComJWTValidationException">
    /// Thrown when validation fails for the token signature or any of the claim
    /// values fall out of spec.
    /// </exception>
    Task<RentingComIdentity> ValidateJWTTokenAsync(string jwtText);
  }
}
