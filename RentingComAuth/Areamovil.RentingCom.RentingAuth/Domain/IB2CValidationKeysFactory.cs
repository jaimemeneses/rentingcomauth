﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Areamovil.RentingCom.RentingAuth.Domain
{
    public interface IB2CValidationKeysFactory
    {
        Task<IEnumerable<RsaSecurityKey>> GetKeysAsync();
    }
}
