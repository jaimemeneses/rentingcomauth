﻿using Areamovil.RentingCom.RentingAuth.Domain;
using Areamovil.RentingCom.RentingAuth.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Areamovil.RentingCom.RentingAuth.AspNet
{
    public static class RentingAuthControllerExtensions
    {
        private const string RentingComIdentityKey = "Areamovil.RentingCom.RentingAuth.Identity";

        public static RentingComIdentity GetRentingComIdentity(this Controller controller)
        {
            if (controller == null)
            {
                throw new ArgumentNullException(nameof(controller));
            }

            if (!controller.HttpContext.Items.ContainsKey(RentingComIdentityKey))
            {
                throw new InvalidOperationException("The user has not been authenticated");
            }

            var identityObject = controller.HttpContext.Items[RentingComIdentityKey];
            var identity = identityObject as RentingComIdentity;
            if (identity == null)
            {
                throw new InvalidOperationException("The user has not been authenticated");
            }

            return identity;
        }

        internal static void InjectRentingComIdentity(IHttpContextAccessor contextAccessor, RentingComIdentity identity)
        {
            if (contextAccessor == null)
            {
                throw new ArgumentNullException(nameof(contextAccessor));
            }

            if (identity == null)
            {
                throw new ArgumentNullException(nameof(identity));
            }

            var httpContext = contextAccessor.HttpContext;
            httpContext.Items[RentingComIdentityKey] = identity;
        }
        public static void AddRentingComIdentityAccesor(this IServiceCollection services)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }
            services.AddSingleton<IRentingComIdentityAccesor, RentingComIdentityAccesor>();
        }
        }
}