﻿using Areamovil.RentingCom.RentingAuth.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Areamovil.RentingCom.RentingAuth.AspNet {
  public static class RentingAuthPolicies {
    public const string RequireAuthentication = "RequireAuthentication";
    public const string RequireServiceAgent = "RequireServiceAgent";
    public const string RequireSalesRep = "RequireSalesRep";
    public const string RequireAdmin = "RequireAdmin";

        public static void AddRentingComAuthorizationPolicies(this IServiceCollection services)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            services.AddAuthorization(options =>
            {
                options.AddPolicy(RequireAuthentication, policy =>
                  policy.Requirements.Add(new RentingComRoleRequirement(RentingComRole.EndUser)));
            });
            services.AddAuthorization(options =>
            {
                options.AddPolicy(RequireSalesRep, policy =>
                  policy.Requirements.Add(new RentingComRoleRequirement(RentingComRole.SalesRep)));
            });
            services.AddAuthorization(options =>
            {
                options.AddPolicy(RequireAdmin, policy =>
                  policy.Requirements.Add(new RentingComRoleRequirement(RentingComRole.AdminAssistant)));
            });
            services.AddAuthorization(options =>
            {
                options.AddPolicy(RequireServiceAgent, policy =>
                  policy.Requirements.Add(new RentingComRoleRequirement(RentingComRole.ServiceAgent)));
            });

            services.AddSingleton<IAuthorizationHandler, RentingComRoleRequirementHandler>();
        }
  }
}
