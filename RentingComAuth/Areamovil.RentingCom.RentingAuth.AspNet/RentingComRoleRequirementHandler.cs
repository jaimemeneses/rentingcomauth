﻿using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using Areamovil.RentingCom.RentingAuth.Domain;
using Microsoft.AspNetCore.Mvc;
using Areamovil.RentingCom.RentingAuth.Models;
using Microsoft.AspNetCore.Http;

namespace Areamovil.RentingCom.RentingAuth.AspNet
{
    internal class RentingComRoleRequirementHandler : AuthorizationHandler<RentingComRoleRequirement> {
        private IRentingComJWTValidator rentingComJWTValidator;
        private IHttpContextAccessor contextAccessor;

        public RentingComRoleRequirementHandler(
            IRentingComJWTValidator rentingComJWTValidator, IHttpContextAccessor contextAccessor) {
            this.rentingComJWTValidator = rentingComJWTValidator;
            this.contextAccessor = contextAccessor;
        }
        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, RentingComRoleRequirement requirement) {
            var resource = ((AuthorizationFilterContext)context.Resource).HttpContext.Request.Headers;
            if (resource.TryGetValue("Authorization", out var jwtText))
            {
                try
                {
                    RentingComIdentity rentingComIdentity = await rentingComJWTValidator.ValidateJWTTokenAsync(jwtText.ToString().Substring(7));

                    if (rentingComIdentity.Role == requirement.RequiredRole)
                    {
                        context.Succeed(requirement);
                        RentingAuthControllerExtensions.InjectRentingComIdentity(contextAccessor, rentingComIdentity);
                    }
                    else
                    {
                        context.Fail();
                    }
                }
                catch (Exception e) {
                    context.Fail();
                }
            }
            else {
                context.Fail();
            }

            return ;
        }

    }
}
