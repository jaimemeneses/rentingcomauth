﻿using Areamovil.RentingCom.RentingAuth.Models;
using Microsoft.AspNetCore.Mvc;

namespace Areamovil.RentingCom.RentingAuth.Domain
{
    public interface IRentingComIdentityAccesor
    {
         RentingComIdentity GetCurrentRentingComIdentity(Controller controller);
        
    }
}