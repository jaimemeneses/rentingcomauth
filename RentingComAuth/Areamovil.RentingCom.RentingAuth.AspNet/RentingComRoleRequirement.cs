﻿using Areamovil.RentingCom.RentingAuth.Models;
using Microsoft.AspNetCore.Authorization;

namespace Areamovil.RentingCom.RentingAuth.AspNet {
  internal class RentingComRoleRequirement : IAuthorizationRequirement {
    public RentingComRole RequiredRole { get; }

    public RentingComRoleRequirement(RentingComRole requiredRole) {
      RequiredRole = requiredRole;
    }
  }
}