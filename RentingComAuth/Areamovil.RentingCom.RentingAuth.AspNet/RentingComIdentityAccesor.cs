﻿using Areamovil.RentingCom.RentingAuth.AspNet;
using Areamovil.RentingCom.RentingAuth.Models;
using Microsoft.AspNetCore.Mvc;

namespace Areamovil.RentingCom.RentingAuth.Domain
{
    public  class RentingComIdentityAccesor: IRentingComIdentityAccesor
    {
        public RentingComIdentity GetCurrentRentingComIdentity(Controller controller) {
            return controller.GetRentingComIdentity();
        }
       
    }
}