﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Areamovil.RentingCom.RentingAuth.Domain;
using Microsoft.AspNetCore.Authorization;
using Areamovil.RentingCom.RentingAuth.AspNet;

namespace WebApplication2.Controllers
{
    [Route("api/[controller]")]
    public class DefaultController : Controller
    {
        private readonly IRentingComJWTValidator rentingComJWTValidator;
        public DefaultController(IRentingComJWTValidator rentingComJWTValidator) {
            this.rentingComJWTValidator = rentingComJWTValidator;
        }
        [Authorize(Policy = RentingAuthPolicies.RequireAuthentication)]
        public async Task<IActionResult> Index()
        {
            string token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ilg1ZVhrNHh5b2pORnVtMWtsMll0djhkbE5QNC1jNTdkTzZRR1RWQndhTmsifQ.eyJpc3MiOiJodHRwczovL2xvZ2luLm1pY3Jvc29mdG9ubGluZS5jb20vZmY2ZDRjMmQtYmIzNy00N2Q2LWE1ODctMmNmYTYwMTRmMThjL3YyLjAvIiwiZXhwIjoxNTE2ODI5MjQzLCJuYmYiOjE1MTY4MjU2NDMsImF1ZCI6ImZjOGIwNDY5LTQwMGQtNDJmYi1iYjgyLTViMTFiMGI4MjhlNiIsIm5hbWUiOiJKYWltZSBIZXJuZXkgcnVpeiIsImlkcCI6Imdvb2dsZS5jb20iLCJzdWIiOiJhM2E2NWY1ZS04OGJiLTRmNDktOGYyZi03N2E5MjJjNTQ1MjQiLCJlbWFpbHMiOlsiZ2Rlc2NhcmdhczExQGdtYWlsLmNvbSJdLCJ0ZnAiOiJCMkNfMV9EZWZhdWx0QXV0aCIsIm5vbmNlIjoiNjM2NTI0MjI0MzEyODE0MDEyLll6bGxNekF5TjJVdFltRTVPUzAwTURFeUxUZzRNVFV0TldZMk1HWTRNVE0wWWpObU1XRmtNRGd6WlRrdE56WTBOaTAwTnpBMUxUZzRZakl0TWpNME0yRTNNVE0zTmpFMCIsInNjcCI6InJlYWQiLCJhenAiOiI1NDYzNTU2My04OGNhLTQyYTMtYmQ2OC04MTEyNzQ3NmE0YmYiLCJ2ZXIiOiIxLjAiLCJpYXQiOjE1MTY4MjU2NDN9.VnNBLiPI8NbDBS7THcX46BFKIU9YMf3GjrTscTouyLqJaRblPkO_IDltgvabML96Q-rZi_y0fDmHh9Utpv8NcCR4wGJT4ZiYqQWnDGHoZUa1j3J89MQJA_NWFQV05WI6FtUDQxhGFXZPDjn4a7ehR1HfzOkuLMZ0iaNNRM5uNpcIo9e8MlpCuaH2GmXkj50CLsOU64l2nq64bX12jO5PF8P2pwb8rqYKicNU5Rto1Mi9e2erBhKE1zhDcYYXyKCq0MsQ7G3MHeaJGznOi53QrIPLREFDFjSppEJyTJB0RzGG84eXc79NuA5HzlyJbAt0DvKVRgMNPbq-pcdMwNaPmQ";
            var r = await rentingComJWTValidator.ValidateJWTTokenAsync(token);
            if (r == null)
            {
                Console.WriteLine("Invalid token");
            }
            else {
                Console.WriteLine(r.Name+"\n"+r.Role + "\n"+r.EmailAddress + "\n"+r.UserId);
                
            }
            return View();
        }
    }
}