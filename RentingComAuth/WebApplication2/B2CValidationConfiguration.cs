﻿using System.Collections.Generic;
using Areamovil.RentingCom.RentingAuth.Domain.ADB2C;

namespace WebApplication2
{
    internal class B2CValidationConfiguration: IB2CValidationConfiguration
    {
        public B2CValidationConfiguration()
        {
            B2CAudiences = new List<string>() { "fc8b0469-400d-42fb-bb82-5b11b0b828e6", "fc8b0469-400d-42fb-bb82-5b11b0b828e7", "fc8b0469-400d-42fb-bb82-5b11b0b828e8" };
            B2CTokenIssuer = "https://login.microsoftonline.com/ff6d4c2d-bb37-47d6-a587-2cfa6014f18c/v2.0/";
            B2CKeysUrl = "https://login.microsoftonline.com/areamovilad.onmicrosoft.com/discovery/v2.0/keys?p=b2c_1_defaultauth";
        }

        public IEnumerable<string> B2CAudiences { get; }

        public string B2CTokenIssuer { get; }

        public string B2CKeysUrl { get; }
    }
}